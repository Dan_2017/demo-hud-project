/**
 * @description: Anonimous Users sessions
 * @name: UserSessions
 * @param: id
 */

var jwt = require("jsonwebtoken");
var crypto = require("crypto");
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSessions = new Schema({
  createdAt: Number,
  uuid: String,
  data: Object,
  password_hash: String,
  salt: String,
});

UserSessions.virtual("password")
  .get(function () {
    return this._password;
  })
  .set(function (password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.password_hash = this.encryptPassword(password);
  });

UserSessions.methods = {
  encryptPassword: function (password) {
    return crypto.createHmac("sha1", this.salt).update(password).digest("hex");
  },
  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashed_password;
  },
  makeSalt: function () {
    return Math.round(new Date().valueOf() * Math.random()) + "";
  },
};

UserSessions.statics.auth = function (password, user) {
  console.log("[user]", user);
  var encryptedPassword = crypto
    .createHmac("sha1", user.salt)
    .update(password)
    .digest("hex");
  return encryptedPassword === user.password_hash
    ? {
        token: jwt.sign(
          {
            exp: 5 * 60 * 1000,
            data: user,
          },
          user.salt
        ),
        secret: user.salt,
      }
    : false;
};

UserSessions.pre("save", function (next) {
  var now = new Date();
  this.createdAt = now.getTime();

  next();
});

module.exports = mongoose.model("UserSessions", UserSessions);
