/**
 * @description:  model
 * @name: PaypalIPNVerificationRequest
 * @param: id, customerId,
 */

var uuid = require("uuid");

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var PaypalIPNVerificationRequest = new Schema({
  // _id: {
  //   type: String,
  //   unique: true,
  // },
  createdAt: Number,
  response: String,
});

PaypalIPNVerificationRequest.pre("save", function (next) {
  var now = new Date();
  this.createdAt = now.getTime();
  // this._id = 'ipnv_' + uuid();

  next();
});

module.exports = mongoose.model(
  "PaypalIPNVerificationRequest",
  PaypalIPNVerificationRequest
);
