/**
 * @description: Paypal IPN message model
 * @name: PaypalIPNMessage
 * @param: id
 */

var uuid = require("uuid");

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var PaypalIPNMessage = new Schema({
  // _id: {
  //   type: String,
  //   unique: true,
  // },
  createdAt: Number,
  response: String,
});

PaypalIPNMessage.pre("save", function (next) {
  var now = new Date();
  this.createdAt = now.getTime();
  // this._id = 'ipn_' + uuid.v1();

  next();
});

module.exports = mongoose.model("PaypalIPNMessage", PaypalIPNMessage);
