/*jshint esversion: 6 */
var express = require("express");
var path = require("path");
var router = express.Router();

require("dotenv").config({
  path: path.join(__dirname, "..", ".env"),
});

console.log("[ENV]", process.env.NODE_ENV);

module.exports = function (app) {
  var module = {};

  router.get("/buy", function (req, res, next) {
    // create payment object
    // console.log('[BUY]', req.query, process.env);

    var payment = {
      intent: "authorize",
      payer: {
        payment_method: "credit_card", //'paypal',
      },
      redirect_urls: {
        return_url: process.env.PAYPAL_RETURN_URL,
        cancel_url: process.env.PAYPAL_CANCEL_URL,
      },
      transactions: [
        {
          amount: {
            total: req.query.amount,
            currency: "USD",
          },
          item_list: {
            items: [
              {
                name: "Order",
                description: "Order at webcard.site",
                quantity: "1",
                price: req.query.amount,
                tax: "0.00",
                sku: "1",
                currency: "USD",
              },
            ],
          },
          description: "subscription",
        },
      ],
    };

    // call the create Pay method
    createPay(payment, app.locals.paypal)
      .then((transaction) => {
        // var id = transaction.id;
        var links = transaction.links;
        var counter = links.length;
        while (counter--) {
          if (links[counter].method === "REDIRECT") {
            // redirect to paypal where user approves the transaction
            // return res.redirect(links[counter].href)
            res.json({
              paypal: {
                status: "success",
                url: links[counter].href,
              },
            });
          }
        }
      })
      .catch((err) => {
        console.log("[err]", err);
        // res.redirect('/err');
        res.json({
          paypal: {
            status: "error",
            error: err,
          },
        });
      });
  });

  router.post("/payment", async function (req, res, next) {
    var axios = require("axios");
    var querystring = require("querystring");

    // Auth URL
    var authUrl = process.env.PAYPAL_AUTH_URL;

    // Auth config
    var authConfig = {
      auth: {
        username: process.env.PAYPAL_CLIENT_ID,
        password: process.env.PAYPAL_CLIENT_SECRET,
      },
      headers: {
        Accept: "application/json",
        "Accept-Language": "en_US",
      },
    };

    // Auth data
    var authData = querystring.stringify({
      grant_type: "client_credentials",
    });

    var getAuthToken = async function () {
      var data = await axios
        .post(authUrl, authData, authConfig)
        .then(function (response) {
          return response.data;
        })
        .catch(function (error) {
          return error;
        });
      return data;
    };

    // Payment URL
    var checkPaymentUrl = process.env.PAYPAL_CHECKOUT_URL;

    // Payment config
    var checkPaymentConfig = {
      headers: {
        Accept: "application/json",
        Authorization: "Bearer ",
      },
    };

    var checkPayment = async function (token, paymentId) {
      checkPaymentConfig.headers.Authorization += token;

      var data = await axios
        .post(checkPaymentUrl + paymentId + "/capture", {}, checkPaymentConfig)
        .then(function (response) {
          return response;
        })
        .catch(function (error) {
          return error;
        });
      return data;
    };

    var makePaymentCheck = async function (id) {
      var response = await getAuthToken();
      var paymentCheck = await checkPayment(response.access_token, id);
      return paymentCheck;
    };

    var result = await makePaymentCheck(req.body.details.id);
    console.log("[RESULT]", result);

    if (process.env.NODE_ENV === "local") {
      res.header("Access-Control-Expose-Headers", "UserStatus, Set-Cookie");
      res.header("Access-Control-Allow-Origin", "http://127.0.0.1:3000");
      res.header("Access-Control-Allow-Headers", "content-type");
      res.header("Access-Control-Allow-Credentials", "true");
    }

    res.json({
      route: "payment",
      data: result.data,
    });
  });

  module.router = router;

  return module;
};

var createPay = (payment, paypal) => {
  return new Promise((resolve, reject) => {
    paypal.payment.create(payment, function (err, payment) {
      if (err) {
        console.log("[err]", err);
        reject(err);
      } else {
        resolve(payment);
      }
    });
  });
};
