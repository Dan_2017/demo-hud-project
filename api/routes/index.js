var express = require("express");
var uuid = require("uuid");
var router = express.Router();
var path = require("path");

require("dotenv").config({
  path: path.join(__dirname, "..", ".env"),
});

/* GET root route. */
router.get("/", function (req, res, next) {
  if (process.env.NODE_ENV !== "local") {
    res.sendFile(path.resolve(__dirname, "..", "public/index.html"));
  } else {
    res.json({
      title: "index",
    });
  }
});

router.get("/hud", function (req, res, next) {
  res.sendFile(path.resolve(__dirname, "..", "public/index.html"));
});

router.get("/pug", function (req, res, next) {
  res.render("index", {
    title: "Express",
    views: req.session.views,
    uuid: req.session.uuid,
    ipInfo: JSON.stringify(req.session.ipInfo),
  });
});

router.get("/fx", function (req, res, next) {
  res.json({ status: "index" });
});

module.exports = router;
