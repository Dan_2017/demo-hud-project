curl -v -X POST https://api.paypal.com/v1/catalogs/products \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer A21AAGPxCGJoBbk_0J2KXInlJSgd1EBQKh39dMTAa5_MF4EsREo97Z0tRzjK9SFYDz7tGl0EM-DRU74f-3mOWbDjfo1J6RwCw" \
  -H "PayPal-Request-Id: <merchant-generated-ID>" \
-d '{
  "name": "Video Streaming Service",
  "description": "Video streaming service",
  "type": "SERVICE",
  "category": "SOFTWARE",
  "image_url": "https://example.com/streaming.jpg",
  "home_url": "https://example.com/home"
}'

# {"id":"PROD-6UB91583720502615","name":"Video Streaming Service","description":"Video streaming service","create_time":"2020-09-13T17:35:16Z","links":[{"href":"https://api.paypal.com/v1/catalogs/products/PROD-6UB91583720502615","rel":"self","method":"GET"},{"href":"https://api.paypal.com/v1/catalogs/products/PROD-6UB91583720502615","rel":"edit","method":"PATCH"}]}