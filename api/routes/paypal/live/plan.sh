curl -v -k -X POST https://api.paypal.com/v1/billing/plans \
  -H "Accept: application/json" \
  -H "Authorization: Bearer A21AAGPxCGJoBbk_0J2KXInlJSgd1EBQKh39dMTAa5_MF4EsREo97Z0tRzjK9SFYDz7tGl0EM-DRU74f-3mOWbDjfo1J6RwCw" \
  -H "PayPal-Request-Id: PLAN-18062020-002" \
  -H "Prefer: return=representation" \
  -H "Content-Type: application/json" \
  -d '{
      "product_id": "PROD-6UB91583720502615",
      "name": "Basic Plan",
      "description": "Basic plan",
      "billing_cycles": [
        {
          "frequency": {
            "interval_unit": "WEEK",
            "interval_count": 1
          },
          "tenure_type": "TRIAL",
          "sequence": 1,
          "total_cycles": 1
        },
        {
          "frequency": {
            "interval_unit": "WEEK",
            "interval_count": 1
          },
          "tenure_type": "REGULAR",
          "sequence": 2,
          "total_cycles": 5,
          "pricing_scheme": {
            "fixed_price": {
              "value": "1",
              "currency_code": "USD"
            }
          }
        }
      ],
      "payment_preferences": {
        "auto_bill_outstanding": true,
        "setup_fee": {
          "value": "1",
          "currency_code": "USD"
        },
        "setup_fee_failure_action": "CONTINUE",
        "payment_failure_threshold": 3
      },
      "taxes": {
        "percentage": "0",
        "inclusive": false
      }
    }'


# {"id":"P-7GX65666T8216714RL5PFSQQ","product_id":"PROD-6UB91583720502615","name":"Basic Plan","status":"ACTIVE","description":"Basic plan","usage_type":"LICENSED","billing_cycles":[{"frequency":{"interval_unit":"WEEK","interval_count":1},"tenure_type":"TRIAL","sequence":1,"total_cycles":1},{"pricing_scheme":{"version":1,"fixed_price":{"currency_code":"USD","value":"0.1"},"create_time":"2020-09-13T17:39:14Z","update_time":"2020-09-13T17:39:14Z"},"frequency":{"interval_unit":"WEEK","interval_count":1},"tenure_type":"REGULAR","sequence":2,"total_cycles":5}],"payment_preferences":{"service_type":"PREPAID","auto_bill_outstanding":true,"setup_fee":{"currency_code":"USD","value":"0.1"},"setup_fee_failure_action":"CONTINUE","payment_failure_threshold":3},"taxes":{"percentage":"10.0","inclusive":true},"quantity_supported":false,"create_time":"2020-09-13T17:39:14Z","update_time":"2020-09-13T17:39:14Z","links":[{"href":"https://api.paypal.com/v1/billing/plans/P-7GX65666T8216714RL5PFSQQ","rel":"self","method":"GET","encType":"application/json"},{"href":"https://api.paypal.com/v1/billing/plans/P-7GX65666T8216714RL5PFSQQ","rel":"edit","method":"PATCH","encType":"application/json"},{"href":"https://api.paypal.com/v1/billing/plans/P-7GX65666T8216714RL5PFSQQ/deactivate","rel":"self","method":"POST","encType":"application/json"}]}

# {"id":"P-95K22463W6661145EL5PF57A","product_id":"PROD-6UB91583720502615","name":"Basic Plan","status":"ACTIVE","description":"Basic plan","usage_type":"LICENSED","billing_cycles":[{"frequency":{"interval_unit":"WEEK","interval_count":1},"tenure_type":"TRIAL","sequence":1,"total_cycles":1},{"pricing_scheme":{"version":1,"fixed_price":{"currency_code":"USD","value":"1.0"},"create_time":"2020-09-13T18:03:40Z","update_time":"2020-09-13T18:03:40Z"},"frequency":{"interval_unit":"WEEK","interval_count":1},"tenure_type":"REGULAR","sequence":2,"total_cycles":5}],"payment_preferences":{"service_type":"PREPAID","auto_bill_outstanding":true,"setup_fee":{"currency_code":"USD","value":"1.0"},"setup_fee_failure_action":"CONTINUE","payment_failure_threshold":3},"taxes":{"percentage":"0.0","inclusive":false},"quantity_supported":false,"create_time":"2020-09-13T18:03:40Z","update_time":"2020-09-13T18:03:40Z","links":[{"href":"https://api.paypal.com/v1/billing/plans/P-95K22463W6661145EL5PF57A","rel":"self","method":"GET","encType":"application/json"},{"href":"https://api.paypal.com/v1/billing/plans/P-95K22463W6661145EL5PF57A","rel":"edit","method":"PATCH","encType":"application/json"},{"href":"https://api.paypal.com/v1/billing/plans/P-95K22463W6661145EL5PF57A/deactivate","rel":"self","method":"POST","encType":"application/json"}]}