/*jshint esversion: 6 */
var axios = require("axios");
// var querystring = require('querystring');

// Payment URL
var paymentUrl =
  "https://api.sandbox.paypal.com/v1/payments/payment/PAYID-L5VIAXI4MJ15296VG3735014/execute";

// Payment config
var paymentConfig = {
  headers: {
    Accept: "application/json",
    Authorization: "Bearer ",
  },
};

// Payment data
// https://example.com/?paymentId=PAYID-L5VIAXI4MJ15296VG3735014&token=EC-15473031T2709323G&PayerID=JLP5X454MMZ4S
var paymentData = {
  payer_id: "JLP5X454MMZ4S",
};

var execPayment = async function (token) {
  paymentConfig.headers.Authorization += token;
  var data = await axios
    .post(paymentUrl, paymentData, paymentConfig)
    .then(function (response) {
      console.log("[P]", response);
      return response;
    })
    .catch(function (error) {
      console.log("[E]", error, JSON.stringify(error.response.data, "", 2));
      return error;
    });
  return data;
};

var exec = (async function () {
  execPayment(
    "A21AAKkJy5oupmZ2ThHZuXWQ0dnlr6r9jaTPWdttVlezZGTpZ88rRFwiElAmEqfs9j-qnbyRycjDKeme6O2GPUSO4p4wh9ajw"
  );
})();
