/*jshint esversion: 6 */
var axios = require("axios");
var querystring = require("querystring");

// Auth URL
var authUrl = "https://api.sandbox.paypal.com/v1/oauth2/token";

// Auth config
var authConfig = {
  auth: {
    username:
      "AWLELO_VUcBIMfIFYUUbI1gB2UfPbWf58Rmt0grIcRZZy7a--jNg06I7htz-qR0n4i6IJnRa8LHueFX8",
    password:
      "EBUVbPbItUhJi6e93LyAb5d6-8oElbvgQT2_2RyVtiyTMbFG9w1boO_qUaBiflGIBS2X8hCdq7z8Hu34",
  },
  headers: {
    Accept: "application/json",
    "Accept-Language": "en_US",
  },
};

// Auth data
var authData = querystring.stringify({
  grant_type: "client_credentials",
});

var getAuthToken = async function () {
  var data = await axios
    .post(authUrl, authData, authConfig)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error;
    });
  return data;
};

// Payment URL
var paymentUrl = "https://api.sandbox.paypal.com/v1/payments/payment";

// Payment config
var paymentConfig = {
  headers: {
    Accept: "application/json",
    Authorization: "Bearer ",
  },
};

// Payment data
var paymentData = {
  intent: "sale",
  payer: {
    payment_method: "paypal",
  },
  transactions: [
    {
      amount: {
        total: "30.11",
        currency: "USD",
        details: {
          subtotal: "30.00",
          tax: "0.07",
          shipping: "0.03",
          handling_fee: "1.00",
          shipping_discount: "-1.00",
          insurance: "0.01",
        },
      },
      description: "This is the payment transaction description.",
      custom: "EBAY_EMS_90048630024435",
      invoice_number: "48787589673",
      payment_options: {
        allowed_payment_method: "INSTANT_FUNDING_SOURCE",
      },
      soft_descriptor: "ECHI5786786",
      item_list: {
        items: [
          {
            name: "hat",
            description: "Brown color hat",
            quantity: "5",
            price: "3",
            tax: "0.01",
            sku: "1",
            currency: "USD",
          },
          {
            name: "handbag",
            description: "Black color hand bag",
            quantity: "1",
            price: "15",
            tax: "0.02",
            sku: "product34",
            currency: "USD",
          },
        ],
        shipping_address: {
          recipient_name: "Hello World",
          line1: "4thFloor",
          line2: "unit#34",
          city: "SAn Jose",
          country_code: "US",
          postal_code: "95131",
          phone: "011862212345678",
          state: "CA",
        },
      },
    },
  ],
  note_to_payer: "Contact us for any questions on your order.",
  redirect_urls: {
    return_url: "https://example.com",
    cancel_url: "https://example.com",
  },
};

var createPayment = async function (token) {
  paymentConfig.headers.Authorization += token;

  var data = await axios
    .post(paymentUrl, paymentData, paymentConfig)
    .then(function (response) {
      // console.log('[P]', response);
      return response;
    })
    .catch(function (error) {
      // console.log('[E]', error);
      return error;
    });
  return data;
};

// Payment URL
var checkPaymentUrl =
  "https://api.sandbox.paypal.com/v2/checkout/orders/6DW55463JC026793S/capture";
// var checkPaymentUrl = 'https://api.sandbox.paypal.com/v2/payments/payment/6DW55463JC026793S';

// Payment config
var checkPaymentConfig = {
  headers: {
    Accept: "application/json",
    Authorization: "Bearer ",
  },
};

var checkPayment = async function (token) {
  checkPaymentConfig.headers.Authorization += token;

  var data = await axios
    .post(checkPaymentUrl, {}, checkPaymentConfig)
    .then(function (response) {
      // console.log('[P]', response);
      return response;
    })
    .catch(function (error) {
      // console.log('[E]', error);
      return error;
    });
  return data;
};

var makePayment = (async function () {
  var res = await getAuthToken();
  console.log("[DONE]", res.access_token);
  var paymentCheck = await checkPayment(res.access_token);
  console.log(paymentCheck);
  // var payment = await createPayment(res.access_token);
  // console.log(payment.data);
})();
