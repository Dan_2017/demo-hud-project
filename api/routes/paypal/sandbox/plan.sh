curl -v -k -X POST https://api.sandbox.paypal.com/v1/billing/plans \
  -H "Accept: application/json" \
  -H "Authorization: Bearer A21AAK52C0vjAOQbfuQUEcbIuHKGSbZBWlmAuxSwdq_7EvvZ5hp3MKqUgu6aTMAiUb7GiTsO-y-9H22CwSVVdLlgR-DqpQfmA" \
  -H "PayPal-Request-Id: PLAN-18062020-001" \
  -H "Prefer: return=representation" \
  -H "Content-Type: application/json" \
  -d '{
      "product_id": "PROD-9S591266RA2592455",
      "name": "Basic Plan",
      "description": "Basic plan",
      "billing_cycles": [
        {
          "frequency": {
            "interval_unit": "MONTH",
            "interval_count": 1
          },
          "tenure_type": "TRIAL",
          "sequence": 1,
          "total_cycles": 1
        },
        {
          "frequency": {
            "interval_unit": "MONTH",
            "interval_count": 1
          },
          "tenure_type": "REGULAR",
          "sequence": 2,
          "total_cycles": 12,
          "pricing_scheme": {
            "fixed_price": {
              "value": "10",
              "currency_code": "USD"
            }
          }
        }
      ],
      "payment_preferences": {
        "auto_bill_outstanding": true,
        "setup_fee": {
          "value": "10",
          "currency_code": "USD"
        },
        "setup_fee_failure_action": "CONTINUE",
        "payment_failure_threshold": 3
      },
      "taxes": {
        "percentage": "10",
        "inclusive": false
      }
    }'


# {"id":"P-062235499U1376539L5PFCQI","product_id":"PROD-9S591266RA2592455","name":"Basic Plan","status":"ACTIVE","description":"Basic plan","usage_type":"LICENSED","billing_cycles":[{"frequency":{"interval_unit":"MONTH","interval_count":1},"tenure_type":"TRIAL","sequence":1,"total_cycles":1},{"pricing_scheme":{"version":1,"fixed_price":{"currency_code":"USD","value":"10.0"},"create_time":"2020-09-13T17:05:05Z","update_time":"2020-09-13T17:05:05Z"},"frequency":{"interval_unit":"MONTH","interval_count":1},"tenure_type":"REGULAR","sequence":2,"total_cycles":12}],"payment_preferences":{"service_type":"PREPAID","auto_bill_outstanding":true,"setup_fee":{"currency_code":"USD","value":"10.0"},"setup_fee_failure_action":"CONTINUE","payment_failure_threshold":3},"taxes":{"percentage":"10.0","inclusive":false},"quantity_supported":false,"create_time":"2020-09-13T17:05:05Z","update_time":"2020-09-13T17:05:05Z","links":[{"href":"https://api.sandbox.paypal.com/v1/billing/plans/P-062235499U1376539L5PFCQI","rel":"self","method":"GET","encType":"application/json"},{"href":"https://api.sandbox.paypal.com/v1/billing/plans/P-062235499U1376539L5PFCQI","rel":"edit","method":"PATCH","encType":"application/json"},{"href":"https://api.sandbox.paypal.com/v1/billing/plans/P-062235499U1376539L5PFCQI/deactivate","rel":"self","method":"POST","encType":"application/json"}]}