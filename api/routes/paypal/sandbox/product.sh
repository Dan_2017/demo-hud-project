curl -v -X POST https://api.sandbox.paypal.com/v1/catalogs/products \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer A21AAK52C0vjAOQbfuQUEcbIuHKGSbZBWlmAuxSwdq_7EvvZ5hp3MKqUgu6aTMAiUb7GiTsO-y-9H22CwSVVdLlgR-DqpQfmA" \
  -H "PayPal-Request-Id: <merchant-generated-ID>" \
-d '{
  "name": "Video Streaming Service",
  "description": "Video streaming service",
  "type": "SERVICE",
  "category": "SOFTWARE",
  "image_url": "https://example.com/streaming.jpg",
  "home_url": "https://example.com/home"
}'

# {"id":"PROD-9S591266RA2592455","name":"Video Streaming Service","description":"Video streaming service","create_time":"2020-09-13T17:00:47Z","links":[{"href":"https://api.sandbox.paypal.com/v1/catalogs/products/PROD-9S591266RA2592455","rel":"self","method":"GET"},{"href":"https://api.sandbox.paypal.com/v1/catalogs/products/PROD-9S591266RA2592455","rel":"edit","method":"PATCH"}]}