/*jshint esversion: 8 */
var uuid = require("uuid");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var session = require("express-session");
var MongoStore = require("connect-mongo")(session);
var expressip = require("express-ip");
var logger = require("morgan");
var paypal = require("paypal-rest-sdk");

require("dotenv").config({
  path: path.join(__dirname, ".env"),
});

//DB

var db = require("./db");
console.log("[M]", db);
console.log("[Database name]", db.mongoose.connections[0].name);
var db_connection = db.mongoose.connections[0];

var app = express();

// Payments

paypal.configure({
  mode: process.env.PAYPAL_MODE, //sandbox or live
  client_id: process.env.PAYPAL_CLIENT_ID,
  client_secret: process.env.PAYPAL_CLIENT_SECRET,
});
app.locals.paypal = paypal;

// Settings

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

// Middleware

app.use(
  express.urlencoded({
    extended: false,
  })
);

app.use(cookieParser());

app.use(express.json());

app.use(expressip().getIpInfoMiddleware);

// Sessions

var sessionsConfig = {
  secret: "keyboard cat",
  store: new MongoStore({
    mongooseConnection: db_connection,
  }),
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 5 * 60000,
  },
};
if (process.env.NODE_ENV !== "local") {
  app.set("trust proxy", true);
  sessionsConfig.cookie.secure = true;
  sessionsConfig.proxy = true;
}

var setSession = function (req, res, next, force) {
  console.log("[COOKIES]", req.cookies);
  var URLMatched = force ? true : false;
  if (!URLMatched) {
    routesPaths.map(function (url) {
      if (req.originalUrl === url) URLMatched = true;
      return null;
    });
  }
  if (URLMatched) {
    session(sessionsConfig)(req, res, async function () {
      console.log("[RS]", req.session);

      if (req.session) {
        if (!req.session.views) req.session.views = 0;
        req.session.views++;

        if (!req.session.uuid) req.session.uuid = uuid.v1();
        if (!req.session.ipInfo) req.session.ipInfo = req.ipInfo;
        if (!req.session.userAgent)
          req.session.userAgent = req.headers["user-agent"];

        console.log("[USER SESSION]", req.session);
        var user = await db.saveUserSession({
          uuid: req.session.uuid,
          data: {
            ipInfo: req.ipInfo,
            userAgent: req.session.userAgent,
          },
        });

        console.log("[USER STATUS]", user);

        // CORS local
        if (process.env.NODE_ENV === "local") {
          res.header("Access-Control-Expose-Headers", "UserStatus, Set-Cookie");
          res.header("Access-Control-Allow-Origin", "http://127.0.0.1:3000");
          res.header("Access-Control-Allow-Headers", "content-type");
          res.header("Access-Control-Allow-Credentials", "true");
        }
        res.header("UserStatus", JSON.stringify(user));

        if (user.auth) {
          req.session.auth = true;
        }
      }
      next();
    });
  } else {
    next();
  }
};

console.log("[sessionsConfig]", sessionsConfig);

app.use(function (req, res, next) {
  setSession(req, res, next);
});

app.use(logger("dev"));

app.use(express.static(path.join(__dirname, "public")));

// Routes

var routes = {
  "/": require("./routes/index"),
  "/users": require("./routes/users"),
  "/paypal": require("./routes/paypal")(app).router,
};

for (var route in routes) {
  app.use(route, routes[route]);
}

var routesPaths = [];

for (route in routes) {
  for (var i = 0; i < routes[route].stack.length; i++) {
    var Layer = routes[route].stack[i];
    if (route === "/") {
      routesPaths.push(Layer.route.path);
    } else {
      routesPaths.push(
        route + (Layer.route.path === "/" ? "" : Layer.route.path)
      );
    }
  }
}

console.log("[ROUTES PATHS]", routesPaths);

// 404

app.get("*", function (req, res) {
  setSession(
    req,
    res,
    function () {
      res.redirect("/");
    },
    true
  );
});

module.exports = app;
