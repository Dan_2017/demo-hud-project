/*jshint esversion: 8 */
/**
 * Database connect
 */

var path = require("path");
var mongoose = require("mongoose");

var UserSessions = require("./models/userSessions.model");
var PaypalIPNMessage = require("./models/paypalIPNMessages.model");
var PaypalIPNVerificationRequest = require("./models/paypalIPNVerificationRequests.model");
var PaypalPaymentRequest = require("./models/paypalPaymentRequests.model");

require("dotenv").config({
  path: path.join(__dirname, ".env"),
});

var mongoMaxRetries = 100;
var defaultRetryMiliSeconds = 5000;
var mongoRetries = mongoMaxRetries;
var mongooseOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  socketTimeoutMS: 30000,
  readPreference: "secondary",
  auto_reconnect: true,
  keepAlive: 30000,
  connectTimeoutMS: 30000,
};

mongoose.set("useFindAndModify", false);

function mongoDbConnect() {
  console.log("connecting with " + process.env.MONGODB_URI);
  mongoose.connect(process.env.MONGODB_URI, mongooseOptions);
}

mongoDbConnect();

mongoose.connection.on("error", function (err) {
  console.log("[MongoDB connection error]: " + err);
});
mongoose.connection.on("connecting", function () {
  console.log("[Reconnecting to DB]");
});
mongoose.connection.on("connected", function () {
  console.log("[Database connected successfully]");

  // Reset
  mongoRetries = mongoMaxRetries;
  // mongooseOptions.retryMiliseconds = defaultRetryMiliSeconds;

  //Init
  new PaypalIPNMessage({
    response: "[init]",
  }).save(function (err, result) {
    if (err) {
      console.log(err);
    }
    console.log("[paypalIPNMessage] initialized");
  });
  new PaypalIPNVerificationRequest({
    response: "[init]",
  }).save(function (err, result) {
    if (err) {
      console.log(err);
    }
    console.log("[paypalIPNVerificationRequest] initialized");
  });
  new PaypalPaymentRequest({
    response: "[init]",
  }).save(function (err, result) {
    if (err) {
      console.log(err);
    }
    console.log("[paypalPaymentRequest] initialized");
  });
});

mongoose.connection.on("reconnected", function () {
  console.log("[Datbase successfully reconnected]");
});
mongoose.connection.on("disconnected", function () {
  console.log(
    "[Database disconnected] retries left: " +
      mongoRetries +
      " retry delay: " +
      mongooseOptions.retryMiliSeconds
  );
  if (mongoRetries > 0) {
    // Increase back off
    defaultRetryMiliSeconds += 100 * (mongoMaxRetries - mongoRetries);
    mongoDbConnect();
    mongoRetries--;
  } else {
    console.log("[No mongodb connection retries left] Quitting.");
    process.exit(1);
  }
});

process.on("SIGINT", function () {
  mongoose.connection.close(function () {
    console.log(
      "Mongoose default connection disconnected through app termination"
    );
    process.exit(0);
  });
});

var saveUserSession = async function ({ uuid, data }) {
  var status = {};
  await UserSessions.find({
    uuid,
  })
    // .select({
    //     uuid: 1,
    //     data: 1,
    //     salt: 1,
    //     password_hash: 1
    // })
    .then(function (user) {
      console.log("[FOUND]", user);
      if (user.length === 0) {
        const UserSession = new UserSessions({
          uuid,
          data,
          password: uuid,
        });
        UserSession.save().then(() => {
          status = {
            new: true,
            auth: false,
          };
        });
      } else {
        var auth = UserSessions.auth(user[0].uuid, user[0]);
        status = {
          new: false,
          auth: auth,
        };
      }
    });
  return status;
};

module.exports.mongoose = mongoose;
module.exports.saveUserSession = saveUserSession;
