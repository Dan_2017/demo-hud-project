import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import HUD from './components/HUD';
import Chat from './components/Chat';

import './App.css';

function App() {
  return (
    <Router>
      <main className="App">
        <Switch>
          <Route exact path="/" component={Chat} />
          <Route path="/hud" component={HUD} />
          <Route path="*" component={Chat} />
        </Switch>
      </main>
    </Router>
  );
}

export default App;
