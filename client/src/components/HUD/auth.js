/*eslint no-extra-bind: "off"*/
/*eslint no-useless-constructor: "off"*/

import axios from 'axios';
import jwt from 'jsonwebtoken';

export default class Auth {
  constructor() {}

  static async getUser() {
    try {
      const response = await axios.get(process.env.REACT_APP_API_URL, {
        withCredentials: true,
      });
      console.log(response);
      if (response.headers.userstatus !== '{}') {
        let userStatus = JSON.parse(response.headers.userstatus);
        console.log('[userStatus]', userStatus);
        // let token = jwt.verify(userStatus.auth.token, userStatus.auth.secret);
        let token = jwt.decode(userStatus.auth.token);
        console.log('[JWT]', token);
      }
    } catch (error) {
      console.error(error);
    }
  }
}
