/*eslint no-extra-bind: "off"*/
/*eslint no-useless-constructor: "off"*/

import axios from 'axios';
// import jwt from 'jsonwebtoken';

export default class Paypal {
  constructor() {}

  static async catchPayment(details) {
    try {
      const response = await axios.post(
        process.env.REACT_APP_API_URL + '/paypal/payment',
        { details: details },
        {
          withCredentials: true,
        }
      );
      console.log(response);
      //   if (response.headers.userstatus !== '{}') {
      //     let userStatus = JSON.parse(response.headers.userstatus);
      //     console.log('[userStatus]', userStatus);
      //     // let token = jwt.verify(userStatus.auth.token, userStatus.auth.secret);
      //     let token = jwt.decode(userStatus.auth.token);
      //     console.log('[JWT]', token);
      //   }
    } catch (error) {
      console.error(error);
    }
  }
}
