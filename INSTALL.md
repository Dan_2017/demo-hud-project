0. DNS
   add wildcard record for subdomains (A \*.webcard.site 14400 88.85.89.70)

1. OS
   apt-get update
   apt-get upgrade
   apt-get install mc htop nodejs npm

2. NodeJS
   npm i n pm2 yarn -g

3. Nginx
   https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04-ru

   netstat -tulpn | grep node

4. Gitlab
   apt-get install gitlab-runner
   gitlab-runner register

   folder structure & permissions

5. UFW
   apt-get install ufw

   https://www.vultr.com/docs/how-to-configure-ufw-firewall-on-ubuntu-14-04
   https://www.linode.com/docs/security/firewalls/configure-firewall-with-ufw/

6. MongoDB
   apt-get install mongodb

   mongo
   use webcard
   db.createUser({
   user: "webcard",
   pwd: "webcard",
   roles: [ { role: "dbOwner", db: "webcard" } ]
   })

   db.grantRolesToUser(
   "webcard",
   [ { role: "dbOwner", db: "webcard" },
   { role: "dbOwner", db: "admin" },
   { role: "clusterAdmin", db: "admin" } ]
   )

   https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-18-04-ru

   https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-mongodb-on-ubuntu-16-04#part-two-securing-mongodb

   https://www.fosstechnix.com/tutorial/mongodb/uninstall-mongodb-from-ubuntu-20-04-lts/

   https://www.tecmint.com/monitor-mongodb-performance/

7. Letsencrypt
   apt-get install certbot

   https://websiteforstudents.com/setup-lets-encrypt-wildcard-on-ubuntu-20-04-18-04/

8. Rsync (backup)

   rsync -avzhe ssh root@88.85.89.70:/var/lib/mongodb pro/Webcard/backup/13092020/var/lib

   https://averagelinuxuser.com/backup-and-restore-your-linux-system-with-rsync/

   https://www.tecmint.com/rsync-local-remote-file-synchronization-commands/#:~:text=Copy%20a%20File%20from%20a,option%20and%20perform%20data%20transfer.

   https://unix.stackexchange.com/questions/79678/force-rsync-to-overwrite-files-at-destination-even-if-theyre-newer

   https://andykdocs.de/development/Linux/2013-01-17+Rsync+over+SSH+with+Key+Authentication

9. SSH Acces

   https://askubuntu.com/questions/115151/how-to-set-up-passwordless-ssh-access-for-root-user

   ssh -i ~/.ssh/id_rsa_remote2 root@88.85.89.70
